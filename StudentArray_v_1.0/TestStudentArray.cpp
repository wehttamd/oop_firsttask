#include <iostream>
#include <algorithm>
#include <utility>

#include "CustomArray.h"
#include "Univer.h"

using namespace std;

void main()
{
	setlocale(LC_ALL, "ru");
	
	Student s = Student::create("Oleg", "Ivanov","Sychev").atAge(18).isOnBudget(false).onCourse(2).withScolarship(1000);
	s.printHuman();


	Rector::GetInstance("Anisimov", "Ivan", "Ivanov", 45);
	Rector::GetInstance("Oleg", "Olegovich", "Olegov", 52)->printHuman();// демонстрация синглтона

	Teacher* t1 = new Teacher("Vladislav", "a", "a", 32);
	t1->printHuman();

	Group group("proginzh");
	group.addStudentToGroup(s);
	group.findStudentInGroup("Oleg", "Ivanov", "Sychev");
	group.findStudentInGroup("aaa", "aa",  "aa");
}
