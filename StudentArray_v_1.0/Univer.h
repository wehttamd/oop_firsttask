#include <iostream>
#include <string>
#include <cassert>
#include <utility>
#include <vector>

#include "CustomArray.h"

using namespace std;

struct Datumn { // Структура дата
	int dd;
	int mm;
	int gg;
	string hours;
};

class Grade // Класс оценка
{
	string subject;
	size_t mark;
};


struct FIO {
	string f;
	string i;
	string o;
};

class Lesson // Запись в расписании (поля: дата, аудитория, предмет и фио преподавателя)
{
friend class Teacher;

private :
	Datumn data;
	string classroom;
	FIO teacher;
	string subject;
};

class Human // класс человек(поля : возраст, фио)
{
protected:
	int age;
	FIO fio;
public:
	Human()
	{
	}
	Human(string F, string I, string O)
	{
		fio = { F, I, O };
	}
	Human(string F, string I, string O , int Age)
	{
		fio = { F, I, O };
		age = Age;
	}
	string getName()
	{
		return fio.i;
	}
	string getSurname()
	{
		return fio.f;
	}
	string getPatronymic()
	{
		return fio.o;
	}
	int getAge()
	{
		return age;
	}
	virtual void printHuman()
	{
		std::cout << "name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + std::to_string(age) << std::endl;
	}
};

class StudentBuilder;

class Student : public Human // класс студент( поля: курс, специальность, массив оценок, , находится ли на бюджете, стипендия)
{
private:
	size_t course;
	string specialty;
	CustomArray<Grade> grades;
	bool isOnBudget;
	size_t scolarship;
public:
	friend class StudentBuilder;
	static StudentBuilder create(string F, string I , string O);
	Student() {
	}
	Student(string F, string I, string O): Human(F, I, O), course(1)
	{}
	int getCourse()
	{
		return course;
	}
	bool isStudentOnBudget()
	{
		return isOnBudget;
	}
	bool operator< (const Student& s)
	{
		return (age < s.age);
	}
	virtual void printHuman()
	{
		string f = isOnBudget ? ", on budget" : ", for a fee";
		cout << " { name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + to_string(age) + ", course = " + to_string(course) +  " " + f +  ", with scolarship " + to_string(scolarship) +   "}" << endl;
	}
	bool operator== (const Student& s)
	{
		return (fio.f == s.fio.f && fio.i == s.fio.i && fio.o == s.fio.o);
	}
};

class StudentBuilder // student class builder
{
	Student student;

public:
	StudentBuilder(string F, string I, string O) : student(F,I,O) {}

	operator Student() const { return move(student); }

	StudentBuilder& atAge(size_t Age)
	{
		student.age = Age;
		return *this;
	}

	StudentBuilder& onCourse(size_t Course)
	{
		student.course = Course;
		return *this;
	}

	StudentBuilder& isOnBudget(bool isOnbudget)
	{
		student.isOnBudget = isOnbudget;
		return *this;
	}
	
	StudentBuilder& withScolarship(size_t stipa)
	{
		student.scolarship = stipa;
		return *this;
	}
};

StudentBuilder Student::create(string F, string I, string O) { return StudentBuilder{ F, I, O }; }

class Teacher : public Human // Класс учитель
{
	friend class Rector;
private:
	size_t salary; //зарплата

public:
	Teacher(string F, string I, string O, int Age): Human(F, I, O, Age), salary(0)
	{}

	Teacher(string F, string I, string O, int Age, int Salary) : Human(F, I, O, Age), salary(Salary)
	{}

	virtual void printHuman()
	{
		std::cout << "name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + std::to_string(age) + ", salary = " + std::to_string(salary) << std::endl;
	}
};

class Rector : public Human // Класс ректор(singleton) // может назначать преподавателей, которые управляют студентами
{
protected:
	Rector(string F, string I, string O, int Age)  // конструктор скрыт, чтобы предотвратить создание объекта через оператор new
	{
		age = Age;
		fio = { F,I,O };
	}

	static Rector* rector_;

public:
	Rector(Rector& other) = delete; // нельзя клонировать ректора

	void operator=(const Rector&) = delete;

	static Rector* GetInstance(string F, string I, string O, int Age);
	static Rector* GetInstance();
	void appointATeacher(Teacher teacher)
	{

	}
	virtual void printHuman()
	{
		cout << " { name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + to_string(age)  + "}" << endl;
	}
};

Rector* Rector::GetInstance(string F, string I, string O, int Age)
{
	if (rector_ == nullptr)
	{
		rector_ = new Rector(F, I, O, Age);
	}
	return rector_;
}

Rector* Rector::GetInstance()
{
	return rector_;
}

Rector* Rector::rector_ = nullptr;;

class Group // класс группа, поля: (массив студентов, расписание, название группы)
{
private:
	CustomArray<Student> students;
	CustomArray<Lesson> lessons;
	string groupName;
public:

	friend class Rector; // доступ к приватным полям только у ректора

	Group(string Name, CustomArray<Student> S) : groupName(Name), students(S)
	{
		cout << "Группа " << Name << " Успешно создана" << endl;
	}

	Group(string Name, int count) : groupName(Name)
	{
		students = CustomArray<Student>(count);
		cout << "Группа " << Name << " Успешно создана" << endl;
	}

	Group(string Name) : groupName(Name)
	{
		students = CustomArray<Student>();
		cout << "Группа " << Name << " успешно создана" << endl;
	}

	auto findStudentInGroup(string F, string I, string O) // поиск студента по фио в группе
	{
		auto isFind = std::find(students.begin(), students.end(), Student(F, I, O));
		if (isFind != students.end())
		{
			cout << "Студент " + F + " " + I + " " + O + " найден в группе " + groupName << endl;
			return isFind;
		}
		else
		{
			cout << "Студент " + F + " " + I + " " + O + " не найден в группе " + groupName << endl;
			return isFind;
		}
	}

	void addStudentToGroup(Student s) // добавить студента в группу
	{
		students.insertElem(s);
		cout << "Студент " + s.getSurname() + " " + s.getName() + " " + s.getPatronymic() + " успешно добавлен в группу" << endl;
	}

	void deleteStudentFromGroup(Student s)
	{
		//if (s)
	}

	friend bool operator<(Group& g1, Group& g2)
	{
		if (g1.groupName < g2.groupName)
			return true;
		if (g1.groupName == g2.groupName)
			//if (g1.s.getLength() < g2.s.getLength())
				return true;
		return false;
	}
};
