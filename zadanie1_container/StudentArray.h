#ifndef STUDENTARRAY_H
#define STUDENTARRAY_H

#include <string>
#include <cassert>

class Student
{
private:
	int age;
	std::string name;
public:
	Student() {
	}
	Student(int Age, std::string Name) : age(Age), name(Name) {}
	std::string getName()
	{
		return name;
	}
	int getAge()
	{
		return age;
	}
	void printStudent()
	{
		std::cout << "name = " + name + "; age = " + std::to_string(age) << std::endl;
	}
};


class StudentArray
{

typedef Student value_type;
typedef Student* pointer;
typedef Student& reference;
typedef	std::random_access_iterator_tag iterator_category;
typedef std::ptrdiff_t difference_type;
typedef std::ptrdiff_t distance_type;

private:
	pointer st;
	int s_length;
public:
	class Iterator
	{
	private:
		pointer _value;
	public:
		Iterator(pointer value = pointer()):
		_value(value)
		{}
		Iterator& operator++ () { ++_value; return *this; }
		Iterator operator++ (int) { return Iterator(_value++); }
		Iterator& operator-- () { --_value; return *this; }
		Iterator operator-- (int) {return Iterator(_value--); }
		difference_type operator - (const Iterator& value) const
		{
			return _value - value._value;
		}
		Iterator operator - (distance_type value) const
		{
			return Iterator(_value - value);
		}
		Iterator operator + (distance_type value) const
		{
			return Iterator(_value + value);
		}
		reference operator * ()
		{
			return  *_value;
		}

		pointer operator -> ()
		{
			return _value;
		}

		bool operator < (const Iterator& value) const
		{
			return _value < value._value;
		}

	};

	StudentArray() : s_length(0), st(nullptr) {}
	StudentArray(int length) : s_length(length)
	{
		assert(length >= 0);
		st = length > 0 ? new Student[length] : nullptr;
	}
	~StudentArray()
	{
		delete[] st;
	}

	void erase()
	{
		delete[] st;

		st = nullptr;
		s_length = 0;
	}
	Student& operator[] (int index)
	{
		assert(index >= 0 && index < s_length);
		return st[index];
	}

	int getLength() { return s_length; }

	Iterator begin() { return st; }
	Iterator end() { return st + s_length; }

	void reallocate(int newLength) // Функция reallocate() изменяет размер массива. Все существующие элементы внутри массива будут уничтожены.
	{
		erase();
		if (newLength <= 0)
			return;
		st = new Student[newLength];
		s_length = newLength;
	}

	void resize(int newLength) // Функция resize() изменяет размер массива. Все существующие элементы сохраняются.
	{
		if (newLength <= 0)
		{
			erase();
			return;
		}

		Student* new_array = new Student[newLength];
		if (s_length > 0)
		{
			int elementsToCopy = newLength > s_length ? s_length : newLength;

			for (int i = 0; i < elementsToCopy; ++i)
				new_array[i] = st[i];
		}
		delete[] st;
		st = new_array;
	}

	void insertStudent(Student* s, int index) { // Добавление студента в массив
		assert(index >= 0 && index <= s_length);
		Student* new_array = new Student[s_length + 1];

		for (int i = 0; i < index; ++i) // копируем все  элементы до нужного индекса
			new_array[i] = st[i];

		new_array[index] = *s; // вставляем элемент

		for (int i = index; i < s_length; ++i) // копируем все  элементы после нужного индекса
			new_array[i + 1] = st[i];

		delete[] st;
		st = new_array;
		++s_length;
	}

	void deleteStudent(int index) // Удалить студента по индексу
	{
		assert(index >= 0 && index < s_length);

		// Если это последний элемент массива, то делаем массив пустым и выполняем return
		if (s_length == 1)
		{
			erase();
			return;
		}
		Student* new_array = new Student[s_length - 1];
		for (int i = 0; i < index; ++i)
		{
			new_array[i] = st[i];
		}
		for (int i = index + 1; i < s_length; ++i)
		{
			new_array[i - 1] = st[i];
		}
		delete[] st;
		st = new_array;
		--s_length;
	}
	void sortArrayByAge()
	{
		int j;
		for (int i = 1; i < s_length; i++)
		{
			Student buff = st[i]; 
			for (j = i - 1; j >= 0 && st[j].getAge() > buff.getAge(); j--)
				st[j + 1] = st[j];

			st[j + 1] = buff; 
		}
	}
	void sortArrayByName()
	{
		int j;
		for (int i = 1; i < s_length; i++)
		{
			Student buff = st[i];
			for (j = i - 1; j >= 0 && st[j].getName() > buff.getName(); j--)
				st[j + 1] = st[j];

			st[j + 1] = buff;
		}
	}
};

#endif
