#include <iostream>
#include "StudentArray.h"

using namespace std;

void main()
{
	StudentArray s_arr;


	for (int i = 0; i < 10; i++)
	{
		Student* cur = new Student (rand() % 30 + 18, "Student" + to_string(rand()%100+i));
		s_arr.insertStudent(cur, i);
	}

	auto it = StudentArray::Iterator(s_arr.begin());
	it++->printStudent();
	it->printStudent();

	(++it)->printStudent();
	it->printStudent();
}
