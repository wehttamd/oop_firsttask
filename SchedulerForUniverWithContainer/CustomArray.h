#ifndef CUSTOMARRAY_H
#define CUSTOMARRAY_H

#include <string>
#include <cassert>
#include <utility>
#include <iostream>
#include <type_traits>
#include <new>

template <typename T>
class Iterator
{
public:
	typedef	std::random_access_iterator_tag iterator_category;
	typedef std::ptrdiff_t difference_type;
	typedef std::ptrdiff_t distance_type;
	typedef T value_type;
	typedef T* pointer;
	typedef T& reference;

	Iterator(pointer value) :
		_value(value) {}
	Iterator(const Iterator& it) : _value(it._value) {}

	Iterator& operator++ () { ++_value; return *this; }
	Iterator operator++ (int) { return Iterator(_value++); }
	Iterator& operator-- () { --_value; return *this; }
	Iterator operator-- (int) { return Iterator(_value--); }
	difference_type operator- (const Iterator& value) const
	{
		return _value - value._value;
	}
	Iterator operator- (distance_type value) const
	{
		return Iterator(_value - value);
	}
	Iterator operator+ (distance_type value) const
	{
		return Iterator(_value + value);
	}
	reference operator* ()
	{
		return  *_value;
	}

	pointer operator-> ()
	{
		return _value;
	}

	bool operator== (const Iterator& value) const  //необходимо для spaceship оператора
	{
		return _value == value._value;
	}

	auto operator<=> (const Iterator& value) const
	{
		return _value <=> value._value;
	}

	Iterator& operator= (const Iterator& value)
	{
		// исключаем присваивание самому себе
		if (this != &value)
			_value = value._value;
		return *this;
	}

private:
	pointer _value;
};

template <typename T>
class CustomArray
{

	typedef T value_type;
	typedef T* pointer;
	typedef T& reference;

private:
	pointer st;
	int s_length;
public:
	

	typedef const Iterator<T> const_iterator;

	// Конструкторы
	CustomArray() : s_length(0), st(nullptr) {}
	CustomArray(int length) : s_length(length)
	{
		assert(length >= 0);
		st = length > 0 ? new value_type[length] : nullptr;
	}
	CustomArray(const CustomArray& arr)
	{
		s_length = arr.s_length;
		st = new value_type[s_length];
		// копируем элементы в новый контейнер
		std::copy(arr.st, arr.st + s_length, st);
	}

	// Деструктор
	~CustomArray()
	{
		delete[] st;
	}

	// оператор присвоений
	CustomArray& operator= (const CustomArray& arr)
	{
		// исключаем присваивание самому себе
		if (this != &arr)
		{
			CustomArray tmp(arr);
			swap(tmp, *this);
		}
		return *this;
	}

	void erase()
	{
		delete[] st;

		st = nullptr;
		s_length = 0;
	}

	// Операторы доступа
	reference operator[] (int index)
	{
		assert(index >= 0 && index < s_length);
		return st[index];
	}

	int size() { return s_length; }
	bool empty() { return s_length == 0; }

	// Итераторы начала и конца контейнера
	Iterator<T> begin() { return Iterator(st); }
	Iterator<T> end() { return Iterator(st + s_length); }

	Iterator<T> cbegin() { return const_iterator(st); }
	Iterator<T> cend() { return const_iterator(st + s_length);}

	void reallocate(int newLength) // Функция reallocate() изменяет размер массива. Все существующие элементы внутри массива будут уничтожены.
	{
		erase();
		if (newLength <= 0)
			return;
		st = new value_type[newLength];
		s_length = newLength;
	}

	void resize(int newLength) // Функция resize() изменяет размер массива. Все существующие элементы сохраняются.
	{
		if (newLength <= 0)
		{
			erase();
			return;
		}

		pointer new_array = new value_type[newLength];
		if (s_length > 0)
		{
			int elementsToCopy = newLength > s_length ? s_length : newLength;

			for (int i = 0; i < elementsToCopy; ++i)
				new_array[i] = st[i];
		}
		delete[] st;
		st = new_array;
	}

	void insertElem(value_type s, int index) { // Добавление элемента в массив
		assert(index >= 0 && index <= s_length);
		pointer new_array = new value_type[s_length + 1];

		for (int i = 0; i < index; ++i) // копируем все  элементы до нужного индекса
			new_array[i] = st[i];

		new_array[index] = s; // вставляем элемент

		for (int i = index; i < s_length; ++i) // копируем все  элементы после нужного индекса
			new_array[i + 1] = st[i];

		delete[] st;
		st = new_array;
		++s_length;
	}

	void insertElem(value_type s) { // Добавление элемента в массив(в конец)
		insertElem(s, s_length);
	}

	void deleteElemByIndex(int index) // Удалить элемент по индексу
	{
		assert(index >= 0 && index < s_length);

		// Если это последний элемент массива, то делаем массив пустым и выполняем return
		if (s_length == 1)
		{
			erase();
			return;
		}
		pointer new_array = new value_type[s_length - 1];
		for (int i = 0; i < index; ++i)
		{
			new_array[i] = st[i];
		}
		for (int i = index + 1; i < s_length; ++i)
		{
			new_array[i - 1] = st[i];
		}
		delete[] st;
		st = new_array;
		--s_length;
	}

	int findByValue (value_type value)
	{
		std::for_each(begin(), end(), [&](const auto& item, int c = 0) {if (item == value) return c; c++; });
		return -1;
	}

	void deleteElem(value_type value) // удалить элемент по значению
	{
		std::for_each(begin(), end(), [&](const auto& item, int c = 0) {if (item == value) deleteElemByIndex(c); c++; });
	}

	void swap(CustomArray& first, CustomArray& second)
	{
		std::swap(first.st, second.st);
		std::swap(first.s_length, second.s_length);
	}
};

template <typename T>
std::ostream& operator<<(std::ostream& os, CustomArray<T>& i) // Оператор вывода для массива
{
	if (i.empty())
		return os << "{}";
	os << "{";
	std::for_each(i.begin(), i.end(), [&](const auto& item) {os << item << ", "; });
	return os << *i.end() << "}";
}

#endif
