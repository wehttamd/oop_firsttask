using namespace std;
#include <string>
#include <iostream>
#include "Univer.h";
#include <map>
#include <chrono>

string UNIVER_SUBJECTS[30] = {
	"math", "geometry", "physical_education", "english",
	"algorithmization_and_programming", "object_oriented_programming", "phylosophy", "project_activity", "safety_of_existing",
	"differential_equations", "physics", "mathlogic", "calculus", "compilation_theory" , "chineese", "economics", "sopromat", "probability_theory"
	, "translation", "history", "japaneese", "algebra", "geodesy", "linguistics", "biology" , "chemistry", "anatomy", "geography", "oceanography", "military_center"
};

string UNIVER_SURNAMES[30] =
{
	"Ivanov",  "Sidorov", "Petrov", "Pervukhin", "Pchelkin", "Smirnov", "Tregulov", "Romanov", "Zinovyev", "Bespalov",
	"Skurikhin", "Pinko", "Seleznev", "Efremov", "Chertkov", "Kuznechikov", "Skliznikov", "Sychev" , "Evlakhin", "Bistrushkina",
	"Artemyeva", "Smelovskaya", "Boeva", "Dukhina", "Kalinnikova" , "Bocharova" , "Shakirova", "Berezova", "Chernova", "Petrova"
};

string UNIVER_NAMES[30] =
{
	"Ivan", "Oleg", "Semen", "Pavel", "Egor", "Nikita", "Stas", "Mikhail", "Valeriy", "Evgeniy", "Alexander", "Aleksey",
	"Maksim", "Gennadiy", "Makar", "Petr", "Matvey", "Ilya", "Anton" , "Yaroslav", "Georgiy" , "Vladimir", "Olga", "Margarita",
	"Maria", "Anna", "Alexandra", "Evelina", "Elizaveta", "Nadezhda"
};

static const int UNIVER_AUDITORIES_SIZE = 5;
string UNIVER_AUDITORIES[UNIVER_AUDITORIES_SIZE] = { // список доступных аудиторий
	{"D504" }, {"D512"  }, {"D636"}, {"D228" }, {"D914"}
	/*{"D529" }, {"E313" },
	{"E215" }, {"E627" }, {"E901" }, {"E412" }, {"E529" },{"G412"} , {"G235"},
	{"G116"}, {"G351"}, {"G522"}, {"G533"}, {"F301"}, {"F225"}, {"F625"}, {"F912"},
	{"F404" }, {"F517"}, {"L212" }, {"L425" }, {"L433" }, {"M234" }, {"M412" } , {"M225" }*/
};

static CustomArray<Student> createStudentsPool(size_t n)
{
	CustomArray<Student> students(n);

	for (int i = 0; i < students.size(); i++)
	{
		bool isOnBudget = (rand() % 2 == 0) ? true : false;
		size_t scolarship = isOnBudget ? 3000 : 0;
		size_t gender = rand() % 2; // woman / man
		if (gender == 0)
		{
			students[i] = Student::create(UNIVER_SURNAMES[rand() % 19], UNIVER_NAMES[rand() % 22], UNIVER_NAMES[rand() % 22] + "vich").
				atAge(18 + rand() % 5).isOnBudget(isOnBudget).onCourse(rand() % 4 + 1).withScolarship(scolarship);
		}
		else
		{
			students[i] = Student::create(UNIVER_SURNAMES[19 + rand() % 11], UNIVER_NAMES[22 + rand() % 8], UNIVER_NAMES[rand() % 22] + "vna").
				atAge(18 + rand() % 5).isOnBudget(isOnBudget).onCourse(rand() % 4 + 1).withScolarship(scolarship);
		}
	}

	return students;
}

static CustomArray<Teacher> createTeachersPool(size_t n)
{
	CustomArray<Teacher> teachers(n);

	for (int i = 0; i < teachers.size(); i++)
	{

		size_t salary = rand() % 4 * 10000 + rand() % 10000;
		size_t gender = rand() % 2; // woman / man
		if (gender == 0)
		{
			teachers[i] = Teacher(UNIVER_SURNAMES[19 + rand() % 11], UNIVER_NAMES[22 + rand() % 8], UNIVER_NAMES[rand() % 22] + "vna", rand() % 50 + 25, salary, UNIVER_SUBJECTS[rand() % 30]);
		}
		else
		{
			teachers[i] = Teacher(UNIVER_SURNAMES[rand() % 19], UNIVER_NAMES[rand() % 22], UNIVER_NAMES[rand() % 22] + "vich", rand() % 50 + 25, salary, UNIVER_SUBJECTS[rand() % 30]);
		}
	}

	return teachers;
}


static Group createRandomGroup(int student_count, int subjects_count) // создаем рандомную группу
{
	CustomArray<Student> students = createStudentsPool(student_count);
	CustomArray<string> subjects(subjects_count);

	for (int i = 0; i < subjects_count; i++)
	{
		subjects[i] = UNIVER_SUBJECTS[i % 30];
	}

	Group group(students, subjects);

	return group;
}

static CustomArray<Lesson> createSchedule(Univer& univer) // Создание расписания
{
	
	int preferablePairs = 2;
	int preferableDays = 2;
	CustomArray<Lesson> lessons(STUDY_DAYS * preferablePairs * univer.getTeachers().size());
	// Для каждого преподавателя ставим 2 пары в день , 2 раза в неделюю, если возможно
	// Если у преподавателя уже есть пара на заданное время, то ищем свободное время
	// Если день полностью занят, то ищем другой день

	auto teachers = univer.getTeachers();
	auto classrooms = univer.getClassrooms();
	int pos = 0;
	for (auto teacher : teachers)
	{
		for (int day = 1; day < STUDY_DAYS; day++)
		{
			int dayAtWeeks = 0;

			bool classroomFound = false; // ищем свободную аудиторию
			Classroom suitableClassroom;

			for (int i = 0; i < classrooms.size(); i++)
			{
				if (day == classrooms[i].day && classrooms[i].isBusy == true && !classroomFound)
				{
					classroomFound = true;
					suitableClassroom = classrooms[i];
					classrooms[i].isBusy = false; // занимаем аудиторию
				}
			}
			if (!classroomFound) // если не нашли, то в тимс
			{
				suitableClassroom.day = day;
				suitableClassroom.auditory = "MS Teams";
			}


			// ищем все пары у преподавателя в этот день и ставим пары в незанятое время
			bool dailyPairs[6] = { false, false, false, false, false, false }; // занятость пар, индексы - enum pairs
			auto teacherPairs = teacher.getPairs();

			for (int i = 0; i < teacherPairs.size(); i++)
			{
				if (day == teacherPairs[i].first) // если нашли пару в этот день
					dailyPairs[teacherPairs[i].second] = true;
			}

			//ставим необходимое количество пар, пока есть свободные

			int pairAtDay = 0;
			int j = 0;
			Lesson lesson;
			lesson.setDay(day);

			while (pairAtDay != preferablePairs && j < 6)
			{
				if (dailyPairs[j] == false)
				{
					pairAtDay++;
					lesson.setPair(pairs(j));
					lesson.setClassroom(suitableClassroom);
					lesson.setSubject(teacher.getSpecialty());
					lesson.setTeacherName(teacher.getFIO());

					lessons.insertElem(lesson);

					
					teachers[pos].setPair(day, pairs(j));
				}
				j++;
			}

			if (pairAtDay > 0) // если поставили хотя бы одну пару, засчитываем день как рабочий
			{
				dayAtWeeks++;
			}
			if (dayAtWeeks == preferableDays) // если набралось 2 рабочих дня на неделе, переходим на следующую неделю
			{
				day += 7 - preferableDays;
				dayAtWeeks = 0;
			}
		}
		pos++;
	}

	for (int i = 1; i < lessons.size(); i++)
		for (int j = i; j > 0 && (!(lessons[j - 1] < lessons[j])); j--) 
			// сортируем расписание по дням
			swap(lessons[j - 1], lessons[j]);
	
	univer.setSchedule(lessons);
	univer.setTeachers(teachers);

	return lessons;
}

static CustomArray<Lesson> createScheduleForConcreteGroup(Univer& univer, Group& group)// создаем расписание для конкретной группы
{
	auto subjects = group.getSubjects();
	auto schedules = univer.getSchedule();
	CustomArray<Lesson> lessons(STUDY_DAYS * 1 * int(sixth_pair+1) * 2);

	int preferablePairs = 2;
	// ставим две пары в неделю по предмету, если возможно
	CustomArray<CustomArray<bool>> freeDays(STUDY_DAYS+1);
	
	for (int i = 1; i < freeDays.size(); i++)
	{
		freeDays[i] = CustomArray<bool>(7);
		for (int j = 0; j < 7; j++)
		{
			freeDays[i][j] = false;
		}
	}

	for (auto subject : subjects)
	{
		for (int day = 1; day < STUDY_DAYS; day++)
		{
			int c = 0;
			Lesson lesson;
			for (int k = 0; k < schedules.size(); k++)
			{
				if (day == schedules[k].data.first && schedules[k].subject == subject && !freeDays[schedules[k].data.first][int(schedules[k].data.second)])
					// если есть пара в этот день у учителя, который ведет нужный предмет, и у группы нет другой пары в это время
				{
					freeDays[schedules[k].data.first][int(schedules[k].data.second)] = true;

					lesson.classroom = schedules[k].classroom;
					lesson.data.first = schedules[k].data.first;
					lesson.data.second = schedules[k].data.second;
					lesson.subject = schedules[k].subject;
					lesson.teacherName = schedules[k].teacherName;
					lessons.insertElem(lesson);

					schedules[k].data.first = 0;
					c++;
					if (c == preferablePairs)
						day += 5;
				}
			}
		}
	}

	for (int i = 1; i < lessons.size(); i++)
		for (int j = i; j > 0 && (!(lessons[j - 1] < lessons[j])); j--)
			// сортируем расписание по дням
			swap(lessons[j - 1], lessons[j]);

	DepartmentHead::GetInstance()->setSchedule(group, lessons);
	return lessons;
}
