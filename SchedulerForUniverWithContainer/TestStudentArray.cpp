#include <iostream>
#include <algorithm>
#include <utility>

#include "CustomArray.h"
#include "ScheduleMaker.h"

using namespace std;

int main()
{	

	DepartmentHead::GetInstance("Artemyeva", "Irina","Leonidovna", 50);
	cout << DepartmentHead::GetInstance()->getFIO() << endl;
	DepartmentHead::GetInstance("Ivanov", "Ivan", "Ivanovich", 10); // демонстрация синглтона
	cout << DepartmentHead::GetInstance()->getFIO() << endl;

	Human firstPerson("Olegov", "Semen", "Alexandrovich", 33);
	Human secondPerson("Olegov", "Semen", "Alexandrovich", 33);
	bool eq = equals(firstPerson, secondPerson);

	Teacher t("Ivanov", "Ivan", "Ivanovich", 50);
	t.setSpecialty("chineese");

	Student s = Student::create("Olegov", "Oleg", "Olegovich").atAge(20);
	colorfulPrintHuman(t);
	colorfulPrintHuman(s);
	colorfulPrintHuman(DepartmentHead::GetInstance());
	firstPerson.printHuman();
	
	Group group1 = createRandomGroup(30, 10);
	group1.setGroupName("proginzh");

	Univer univer("Dvfu");
	univer.addGroup(group1);
	univer.setTeachers(createTeachersPool(10));

	for (int i = 1; i < STUDY_DAYS; i++) // создаем свободные аудитории для пар
	{
		for (int j = 0; j < UNIVER_AUDITORIES_SIZE; j++)
		{
			Classroom cr;
			cr.auditory = UNIVER_AUDITORIES[j];
			cr.day = i;
			cr.isBusy = true;
			univer.addClassRoom(cr);
		}
	}
	
	createSchedule(univer); // создание расписания для универа
	univer.printSchedule();

	createScheduleForConcreteGroup(univer, group1); // создание расписания для конкретной группы
	DepartmentHead::GetInstance()->printLessons(group1);

	univer.addTeacher(t); // ивану не платим, он не работает
	auto teachers = univer.getTeachers();
	auto schedule = univer.getSchedule();

	for (int i = 0; i < teachers.size(); i++)
	{
		int c = 0;
		for (auto sch : schedule)
			if (sch.teacherName == teachers[i].getFIO())
			{
				c++;
			}
		if (c >= 2)
			teachers[i].setSalary(c * 1000);
	}

	for (int i = 0; i < teachers.size(); i++)
	{
		colorfulPrintHuman(teachers[i]);
	}

	/*
	for (auto subject : group1.getSubjects())
	{
		cout << subject << " ";
	}*/

	return 0;
}
