#include <iostream>
#include <string>
#include <cassert>
#include <utility>
#include <vector>
#include <conio.h>
#include <stdio.h>

#include "CustomArray.h"

using namespace std;

enum pairs { first_pair = 0, second_pair, third_pair, fourth_pair, fifth_pair, sixth_pair };

typedef std::pair<int, pairs> Datumn;
static int STUDY_DAYS = 30;

class Grade // Класс оценка
{
	string subject = "";
	size_t mark = 0;

public:
	Grade(string Subject, size_t Mark) : subject(Subject), mark(Mark) {}
	Grade() {}

	bool operator== (const Grade& other)
	{
		return (subject == other.subject);
	}
};

struct FIO {

	string f = "";
	string i = "";
	string o = "";

	bool operator== (const FIO& other)
	{
		return (f == other.f && i == other.i && o == other.o);
	}
};

std::ostream& operator<<(std::ostream& os, FIO fio) // Оператор вывода для фио
{
	return os << fio.f + " " + fio.i + " " + fio.o;
}

struct Classroom
{
	int day;
	string auditory;
	bool isBusy;
};

class LessonBuilder;

class Lesson // Запись в расписании (поля: дата, аудитория, предмет и фио преподавателя)
{

public:
	friend class Teacher;
	friend class LessonBuilder;

	Datumn data = {0, first_pair};
	Classroom classroom = {0, "", false};
	FIO teacherName = {"", "", ""};
	string subject = "";

	Lesson() {}
	Lesson(Datumn Data, Classroom Class, FIO TeacherName, string Subject) :
		data(Data), classroom(Class), teacherName(TeacherName), subject(Subject)
	{}

	static LessonBuilder create();

	void setDay(int day)
	{
		data.first = day;
	}

	int getDay()
	{
		return data.first;
	}

	void setPair(pairs pair)
	{
		data.second = pair;
	}

	void setClassroom(Classroom Class)
	{
		classroom = Class;
	}

	void setTeacherName(FIO TeacherName)
	{
		teacherName = TeacherName;
	}

	FIO getTeacherName()
	{
		return teacherName;
	}

	void setSubject(string Subject)
	{
		subject = Subject;
	}

	void printLesson()
	{;
		cout << "{ day : " << int(data.first) << ", pair: " << int(data.second + 1) << ", " << classroom.auditory << ", " << subject << ", " << teacherName.f << " " << teacherName.i << " " << teacherName.o << " }" << endl;
	}

	bool operator== (const Lesson& other)
	{
		return (other.data == data);
	}
	bool operator< (const Lesson& other)
	{
		return (data.first < other.data.first);
	}
};

class Human // класс человек(поля : возраст, фио)
{
protected:
	size_t age = 0;
	FIO fio = { "", "", "" };
public:
	Human()
	{
		age = 0;
	}
	Human(string F, string I, string O)
	{
		fio = { F, I, O };
		age = 0;
	}
	Human(string F, string I, string O , size_t Age)
	{
		fio = { F, I, O };
		age = Age;
	}
	FIO getFIO()
	{
		return fio;
	}
	size_t getAge()
	{
		return age;
	}
	virtual void printHuman()
	{
		std::cout << "name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + std::to_string(age) << std::endl;
	}
	virtual ~Human()
	{
	}
};

template<typename T>
concept Comparable = std::is_same<T, class Human>::value;

template <Comparable X, Comparable Y>

bool equals(X first, Y second)
{
	bool isEquals = first.getAge() == second.getAge() && first.getFIO() == second.getFIO();
	if (isEquals)
	{
		cout << "Person "; first.printHuman(); cout << "equals to person "; second.printHuman();
	}
	else
	{
		cout << "Person ";  first.printHuman(); cout << "is not equals to person "; second.printHuman();
	}
	return isEquals;
}

class StudentBuilder;

class Student : public Human // класс студент( поля: курс, специальность, массив оценок, , находится ли на бюджете, стипендия, название группы)
{
private:
	size_t course = 1;
	string specialty = "defaultSpecialty";
	CustomArray<Grade> grades = NULL;
	bool isOnBudget = true; // по дефолту всех на бюджет
	size_t scolarship = 0;
	string groupname = "";
public:
	friend class StudentBuilder;
	friend class Teacher;

	static StudentBuilder create(string F, string I, string O);
	Student()
	{}
	Student(string F, string I, string O) : Human(F, I, O), course(1)
	{}
	size_t getCourse()
	{
		return course;
	}
	bool isStudentOnBudget()
	{
		return isOnBudget;
	}
	bool operator< (const Student& s)
	{
		return (age < s.age);
	}
	virtual void printHuman()
	{
		string f = isOnBudget ? ", on budget" : ", for a fee";
		cout << " { name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + to_string(age) + ", course = " + to_string(course) + " " + f + ", with scolarship " + to_string(scolarship) + "}" << endl;
	}
	bool operator== (const Student& s)
	{
		return (fio.f == s.fio.f && fio.i == s.fio.i && fio.o == s.fio.o);
	}
	
	CustomArray<Grade> getGrades()
	{
		return grades;
	}

	virtual ~Student()
	{
	}
};

class StudentBuilder // student class builder
{
	Student student;

public:
	StudentBuilder(string F, string I, string O) : student(F,I,O) {}

	operator Student() const { return move(student); }

	StudentBuilder& atAge(size_t Age)
	{
		student.age = Age;
		return *this;
	}

	StudentBuilder& onCourse(size_t Course)
	{
		student.course = Course;
		return *this;
	}

	StudentBuilder& isOnBudget(bool isOnbudget)
	{
		student.isOnBudget = isOnbudget;
		return *this;
	}
	
	StudentBuilder& withScolarship(size_t stipa)
	{
		student.scolarship = stipa;
		return *this;
	}
};

StudentBuilder Student::create(string F, string I, string O) { return StudentBuilder{ F, I, O }; }

class Teacher : public Human // Класс учитель
{
	friend class DepartmentHead;
private:
	size_t salary = 0; //зарплата
	string specialty = "";
	CustomArray <std::pair<int, pairs>> teachers_schedule = NULL;

public:
	Teacher() {
		teachers_schedule = CustomArray<Datumn>(STUDY_DAYS + 1);
		int i = 0;
		for (auto s : teachers_schedule)
		{
			teachers_schedule[i].first = 0;  teachers_schedule[i].second = first_pair;
		}
	};

	Teacher(string F, string I, string O, size_t Age) : Human(F, I, O, Age), salary(0)
	{
		teachers_schedule = CustomArray<Datumn>(STUDY_DAYS+1);
		int i = 0;
		for (auto s : teachers_schedule)
		{
			teachers_schedule[i].first = 0;  teachers_schedule[i].second = first_pair;
		}
	}

	Teacher(string F, string I, string O, size_t Age, size_t Salary, string Specialty) : Teacher(F, I, O, Age)
	{
		salary = Salary;
		specialty = Specialty;
	}

	virtual void printHuman()
	{
		std::cout << "name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + std::to_string(age) + ", salary = " + std::to_string(salary) << std::endl;
	}

	void setSalary(size_t salary)
	{
		this->salary = salary;
	}

	virtual ~Teacher()
	{
	}

	string getSpecialty()
	{
		return specialty;
	}

	void setSpecialty(string Specialty)
	{
		specialty = Specialty;
	}

	void setPair(int day, pairs pair)
	{
		auto scheduleRecord = make_pair(day, pair);
		int pos = teachers_schedule.findByValue(scheduleRecord);
		if (pos >= 0)
			teachers_schedule[pos] = scheduleRecord;
		else
			teachers_schedule.insertElem(scheduleRecord);
	}

	auto getPairs()
	{
		return teachers_schedule;
	}

	void setMark(Student student, size_t mark)
	{
		Grade g(specialty, mark);
		size_t position = student.getGrades().findByValue(g);

		if (position == -1)
		{
			student.getGrades().insertElem(g);
		}
		else
		{
			student.getGrades()[position] = g;
		}
	}
	bool operator== (const Teacher& other)
	{
		return (other.fio == fio && other.age == age);
	}
};

class LessonBuilder
{
private:
	Lesson lesson;
public:
	LessonBuilder() {}

	operator Lesson() const { return move(lesson); }

	LessonBuilder& atDay(int day)
	{
		lesson.data.first = day;
		return *this;
	}

	LessonBuilder& atPair(pairs pair)
	{
		lesson.data.second = pair;
		return *this;
	}

	LessonBuilder& atClass(Classroom classroom)
	{
		lesson.classroom = classroom;
		return *this;
	}
	LessonBuilder& withTeacher(Teacher teacher)
	{
		lesson.setTeacherName({ teacher.getFIO() });
		return *this;
	}

	LessonBuilder& onSubject(string subject)
	{
		lesson.setSubject(subject);
		return *this;
	}
};
LessonBuilder Lesson::create() { return LessonBuilder{}; }

class Group // класс группа, поля: (массив студентов, расписание, название группы)
{
private:
	CustomArray<Student> students = NULL;
	CustomArray<Lesson> lessons = NULL;
	CustomArray<string> subjects = NULL;
	string groupName = "";
public:

	friend class DepartmentHead; // доступ к приватным полям только у главы департамента

	Group()
	{
		students = CustomArray<Student>(10);
		lessons = CustomArray<Lesson>(10);
	}

	Group(CustomArray<Student> Students)
	{
		students = Students;
	}

	Group(CustomArray<Student> Students, CustomArray<string> Subjects)
	{
		students = Students;
		subjects = Subjects;
		lessons = CustomArray<Lesson>(STUDY_DAYS + 1);
		int i = 0;
		for (auto s : lessons)
		{
			lessons[i].data.first = 0;  lessons[i].data.second = first_pair;
		}
	}

	Group(string Name, CustomArray<Student> S) : groupName(Name), students(S)
	{
		cout << "Group " << Name << " was created" << endl;
	}

	Group(string Name, size_t count) : groupName(Name)
	{
		students = CustomArray<Student>(count);
		cout << "Group " << Name << " was created" << endl;
	}

	Group(string Name) : groupName(Name)
	{
		students = CustomArray<Student>();
		cout << "Group " << Name << " was created" << endl;
	}

	void setGroupName(string name)
	{
		groupName = name;
	}

	string getGroupName()
	{
		return groupName;
	}

	friend bool operator<(Group& g1, Group& g2)
	{
		if (g1.groupName < g2.groupName)
			return true;
		if (g1.groupName == g2.groupName)
			//if (g1.s.getLength() < g2.s.getLength())
				return true;
		return false;
	}

	void setSubjects(CustomArray<string> Subjects)
	{
		subjects = Subjects;
	}
	CustomArray<string>& getSubjects()
	{
		return subjects;
	}
};


class DepartmentHead : public Human // руководитель департамента(singleton) 
{
protected:
	DepartmentHead(string F, string I, string O, size_t Age)  // конструктор скрыт, чтобы предотвратить создание объекта через оператор new
	{
		age = Age;
		fio = { F,I,O };
	}

	static DepartmentHead* DepartmentHead_;

public:
	DepartmentHead(DepartmentHead& other) = delete; // нельзя клонировать ректора

	void operator=(const DepartmentHead&) = delete;

	static DepartmentHead* GetInstance(string F, string I, string O, size_t Age);
	static DepartmentHead* GetInstance();

	void addStudentToGroup(Group& group, Student s) // добавить студента в группу
	{
		group.students.insertElem(s);
		cout << "Student " << s.getFIO() << " was added to group " << group.groupName << endl;
	}

	void deleteStudentFromGroup(Group& group, string F, string I, string O) // удалить студента из группы по фио
	{
		auto toDel = std::find(group.students.begin(), group.students.end(), Student(F, I, O));
		if (toDel == group.students.end())
		{
			return;
		}
		else
		{
			group.students.deleteElem(*toDel);
			cout << "Student " + F + " " + I + " " + O + " was deleted from group " << group.groupName << endl;
		}
	}

	void addLessonToGroup(Group& group, Lesson s) // добавить пару у группы
	{
		group.lessons.insertElem(s);
		s.printLesson();
	    cout << " was added to group " << group.groupName << endl;
	}

	void deleteLessonFromGroup(Group& group, Lesson data) // убрать пару у группы
	{
		auto toDel = std::find(group.lessons.begin(), group.lessons.end(), data);
		if (toDel == group.lessons.end())
		{
			return;
		}
		else
		{
			data.printLesson();
			group.lessons.deleteElem(*toDel);
			cout << " was deleted from group " << group.groupName << endl;
		}
	}
	void deleteLessonFromGroup(Group& group, Datumn data) // убрать пару у группы по расписанию
	{
		auto toDel = std::find(group.lessons.begin(), group.lessons.end(), Lesson(data, {}, {}, ""));
		if (toDel == group.lessons.end())
		{
			return;
		}
		else
		{
			group.lessons.deleteElem(*toDel);
			cout << "Day : " << data.first << " Pair : " << data.second+1 << " was deleted from group " << group.groupName << endl;
		}
	}

	void setSchedule(Group& group, CustomArray<Lesson> Lessons)
	{
		group.lessons = Lessons;
	}

	CustomArray<Lesson> getSchedule(Group group)
	{
		return group.lessons;
	}

	void printStudents(Group group)
	{
		cout << "Group " + group.groupName << endl;
		cout << "Students in group : " << endl;
		std::for_each(group.students.begin(), group.students.end(), [](auto& item) {item.printHuman(); });

		cout << endl;
	}

	void printLessons(Group group)
	{
		cout << "Group " + group.groupName << endl;
	
		std::for_each(group.lessons.begin(), group.lessons.end(), [](auto& item) {if (item.getTeacherName().f != "" && item.getDay() > 1) item.printLesson(); });

		cout << endl;
	}

	virtual void printHuman()
	{
		cout << " { name = " + fio.i + ", surname = " + fio.f + ", patronymic = " + fio.o + ", age = " + to_string(age) + "}" << endl;
	}

	virtual ~DepartmentHead()
	{
		cout << "head of department " << fio << " was destroyed ";
	}
};

DepartmentHead* DepartmentHead::GetInstance(string F, string I, string O, size_t Age)
{
	if (DepartmentHead_ == nullptr)
	{
		DepartmentHead_ = new DepartmentHead(F, I, O, Age);
	}
	return DepartmentHead_;
}

DepartmentHead* DepartmentHead::GetInstance()
{
	return DepartmentHead_;
}

DepartmentHead* DepartmentHead::DepartmentHead_ = nullptr;;

class Univer {
private:
	CustomArray<Classroom> classrooms = NULL;
	CustomArray<Group> groups = NULL;
	CustomArray<Lesson> schedule = NULL;
	CustomArray<Teacher> teachers = NULL;
	string name;
public:

	Univer(string Name) : name(Name) {
		schedule = CustomArray<Lesson>(STUDY_DAYS + 1);
		int i = 0;
		for (auto s : schedule)
		{
			schedule[i].data.first = 0;  schedule[i].data.second = first_pair;
		}
	};
	CustomArray<Group> getGroups()
	{
		return groups;
	}

	CustomArray<Classroom>& getClassrooms()
	{
		return classrooms;
	}
	CustomArray<Lesson> getSchedule()
	{
		return schedule;
	}

	void addClassRoom(Classroom cr)
	{
		classrooms.insertElem(cr);
	}

	void addGroup(Group group)
	{
		groups.insertElem(group);
	}

	void addTeacher(Teacher teacher)
	{
		teachers.insertElem(teacher);
	}

	string getName()
	{
		return name;
	}

	void setName(string Name)
	{
		name = Name;
	}

	void printSchedule()
	{
		std::for_each(schedule.begin(), schedule.end(), [](auto& item) {if (item.getTeacherName().f != "" && item.getDay() > 0) item.printLesson(); });
		cout << endl;
	}
	
	void setSchedule(CustomArray<Lesson> Schedule)
	{
		schedule = Schedule;
	}

	CustomArray<Teacher>& getTeachers()
	{
		return teachers;
	}

	void setTeachers(CustomArray<Teacher> Teachers)
	{
		teachers = Teachers;
	}
};


namespace win //  чтобы виндовс аш не ругался на "неоднозначное определение байт" (во всем виноваты юзинги)
{
#include <windows.h>
	typedef byte cs_byte;

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
};

//цветная печать для каждой сущности, для удобства

template<typename U>
concept sameAsTeacher = std::is_same<U, class Teacher>::value;
template <sameAsTeacher X>
void colorfulPrintHuman(X x)
{
	win::SetConsoleTextAttribute(win::hConsole, 2);
	cout << "Teacher " << endl;
	x.printHuman();
	win::SetConsoleTextAttribute(win::hConsole, 7);
}

template<typename U>
concept sameAsStudent = std::is_same<U, class Student>::value;
template <sameAsStudent X>
void colorfulPrintHuman(X x)
{
	win::SetConsoleTextAttribute(win::hConsole, 3);
	cout << "Student " << endl;
	x.printHuman();
	win::SetConsoleTextAttribute(win::hConsole, 7);
}

template<typename U>
concept sameAsDepHead = std::is_same<U, DepartmentHead*>::value;
template <sameAsDepHead X>
void colorfulPrintHuman(X x)
{
	win::SetConsoleTextAttribute(win::hConsole, 4);
	cout << "Head of Department " << endl;
	x->printHuman();
	win::SetConsoleTextAttribute(win::hConsole, 7);
}
